FROM alpine:edge

RUN apk add --update exiftool \
      && rm -rf /var/cache/apk/*

VOLUME /data
WORKDIR /data

ENTRYPOINT ["exiftool"]

