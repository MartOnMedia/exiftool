NAME=mart0n/exiftool
REGISTRY=docker.io
TAG=latest

DOCKER=docker

build:
	$(DOCKER) build --rm --pull -t $(REGISTRY)/$(NAME):$(TAG) .

push:
	$(DOCKER) push $(REGISTRY)/$(NAME):$(TAG)
